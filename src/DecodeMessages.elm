module DecodeMessages exposing (decodeServerMessage, ServerAction(..))

import Json.Decode exposing (..)
import Table exposing (..)


type ServerAction
    = LoginFailed
    | LoginSuccess
    | TableList (List Table)
    | RemovalFailed Int
    | EditFailed Int
    | TableAdded Table Int
    | TableRemoved Int
    | TableUpdated Table



-- newTable = Table False


decodeServerMessage : String -> Result String ServerAction
decodeServerMessage msg =
    decodeString getServerAction msg


getServerAction : Decoder ServerAction
getServerAction =
    (field "$type" string)
        |> andThen constructActionTypeDecoder


newTable : Int -> String -> Int -> Table
newTable =
    Table False


tableDecoder : Decoder Table
tableDecoder =
    map3 newTable
        (field "id" int)
        (field "name" string)
        (field "participants" int)


getId : Decoder Int
getId =
    (field "id" int)


tableIdActionDecoder : (Int -> ServerAction) -> Decoder ServerAction
tableIdActionDecoder act =
    map (\id -> act id) getId


constructActionTypeDecoder : String -> Decoder ServerAction
constructActionTypeDecoder type_ =
    case type_ of
        "login_failed" ->
            succeed LoginFailed

        "login_successful" ->
            succeed LoginSuccess

        "table_list" ->
            map (\tables -> TableList tables)
                (field "tables" <| list tableDecoder)

        "removal_failed" ->
            tableIdActionDecoder RemovalFailed

        "edit_failed" ->
            tableIdActionDecoder EditFailed

        "table_added" ->
            map2 (\afterId table -> TableAdded table afterId)
                (field "after_id" int)
                (field "table" tableDecoder)

        "table_removed" ->
            tableIdActionDecoder TableRemoved

        "table_updated" ->
            map (\table -> TableUpdated table)
                (field "table" tableDecoder)

        _ ->
            fail (type_ ++ " is an unexpected message type from server")
