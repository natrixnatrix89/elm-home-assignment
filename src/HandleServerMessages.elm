module HandleServerMessages exposing (handleServerMessage)

import WebSocket
import DecodeMessages exposing (..)
import Table exposing (..)
import Types exposing (..)
import GenerateMessages exposing (subscribeTables)


handleServerMessage : Model -> String -> ( Model, Cmd Msg )
handleServerMessage model str =
    case decodeServerMessage str of
        Err msg ->
            ( model, autoDismissableError "Could not parse the incoming server message" )

        Ok result ->
            case result of
                LoginFailed ->
                    ( model, autoDismissableError "The entered password doesn't match the user" )

                LoginSuccess ->
                    ( { model | login = True }, WebSocket.send (wsUrl model) subscribeTables )

                TableList tables ->
                    ( { model | tables = tables }, Cmd.none )

                RemovalFailed id ->
                    ( { model
                        | tables =
                            List.map
                                (\table ->
                                    if table.id == id then
                                        { table | beingDeleted = False }
                                    else
                                        table
                                )
                                model.tables
                      }
                    , autoDismissableError ("Table removal " ++ toString id ++ " was unsuccessful")
                    )

                TableRemoved id ->
                    ( { model | tables = List.filter (deleteTableFilter id) model.tables }, Cmd.none )

                EditFailed id ->
                    ( model, Cmd.none )

                TableAdded table after_id ->
                    case insertTable model.tables table after_id of
                        Ok tables ->
                            ( { model | tables = tables }, Cmd.none )

                        Err why ->
                            ( model, autoDismissableError why )

                TableUpdated table ->
                    let
                        mapTable tb =
                            if tb.id == table.id then
                                { tb | name = table.name, participants = table.participants }
                            else
                                tb

                        t =
                            List.map mapTable model.tables
                    in
                        ( { model | tables = t }, Cmd.none )


deleteTableFilter : TableId -> Table -> Bool
deleteTableFilter id table =
    id /= table.id


insertTable : List Table -> Table -> TableId -> Result String (List Table)
insertTable tables tableToAdd afterId =
    if afterId == -1 then
        Ok (tableToAdd :: tables)
    else
        let
            iterate : List Table -> List Table -> Result String (List Table)
            iterate acc tables =
                case tables of
                    [] ->
                        Err "Such table_id could not be found"

                    x :: xs ->
                        if x.id == afterId then
                            Ok (List.foldl (::) (x :: tableToAdd :: xs) acc)
                        else
                            iterate (x :: acc) xs
        in
            iterate [] tables
