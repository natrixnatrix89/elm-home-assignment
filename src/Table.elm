module Table exposing (..)


type alias TableId =
    Int


type alias Table =
    { beingDeleted : Bool
    , id : TableId
    , name : String
    , participants : Int
    }
