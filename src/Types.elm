module Types exposing (..)

import Time exposing (Time, now, second)
import Table exposing (..)
import Task
import Navigation exposing (Location)


type alias ErrorMessage =
    { dismissAt : Time
    , content : String
    }


type alias Model =
    { login : Bool
    , user : String
    , password : String
    , tables : List Table
    , tableForm : Table
    , error : Maybe ErrorMessage
    , location : Location
    }


type Msg
    = TypeUser String
    | TypePass String
    | Login
    | DeleteTable Int
    | DismissError
    | NewError Time String
    | NewMessage String
    | Tick Time
    | UrlChange Location


autoDismissableError : String -> Cmd Msg
autoDismissableError msg =
    Task.perform (\time -> NewError (time + second * 5) msg) Time.now


wsUrl : Model -> String
wsUrl model =
    "ws://" ++ model.location.host ++ "/ws_api"
