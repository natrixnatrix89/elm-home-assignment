module GenerateMessages exposing (login, subscribeTables, addTable, deleteTable)

import Json.Encode exposing (..)
import Table exposing (..)


whitespace : Int
whitespace =
    2


login : String -> String -> String
login user pass =
    encode whitespace <|
        object
            [ ( "$type", string "login" )
            , ( "username", string user )
            , ( "password", string pass )
            ]


subscribeTables : String
subscribeTables =
    encode whitespace <|
        object
            [ ( "$type", string "subscribe_tables" ) ]


addTable : Table -> String
addTable table =
    encode whitespace <|
        object
            [ ( "$type", string "add_table" )
            , ( "table"
              , object
                    [ ( "id"
                      , int table.id
                      )
                    , ( "name", string table.name )
                    , ( "participants", int table.participants )
                    ]
              )
            ]


deleteTable : Int -> String
deleteTable id =
    encode whitespace <|
        object
            [ ( "$type", string "remove_table" )
            , ( "id", int id )
            ]
