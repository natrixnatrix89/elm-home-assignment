module Main exposing (main)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import WebSocket
import Table exposing (..)
import GenerateMessages exposing (..)
import Time exposing (every, second)
import Platform.Sub
import Types exposing (..)
import HandleServerMessages exposing (handleServerMessage)
import Navigation


main : Program Never Model Msg
main =
    Navigation.program UrlChange
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }



-- MODEL


init : Navigation.Location -> ( Model, Cmd Msg )
init location =
    ( Model
        False
        "user1234"
        "password1234"
        []
        (Table False -1 "" 0)
        Nothing
        location
    , Cmd.none
    )



-- UPDATE


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        TypeUser user ->
            ( { model | user = user }, Cmd.none )

        TypePass pass ->
            ( { model | password = pass }, Cmd.none )

        Login ->
            ( { model | user = "", password = "" }, WebSocket.send (wsUrl model) (login model.user model.password) )

        DeleteTable id ->
            ( { model
                | tables =
                    List.map
                        (\t ->
                            if t.id /= id then
                                t
                            else
                                { t | beingDeleted = True }
                        )
                        model.tables
              }
            , WebSocket.send (wsUrl model) (deleteTable id)
            )

        DismissError ->
            ( { model | error = Nothing }, Cmd.none )

        NewError when what ->
            ( { model | error = Just <| ErrorMessage when what }, Cmd.none )

        NewMessage str ->
            handleServerMessage model str

        Tick time ->
            -- Dismiss model errors after 5 seconds
            case model.error of
                Just err ->
                    if err.dismissAt < time then
                        ( { model | error = Nothing }, Cmd.none )
                    else
                        ( model, Cmd.none )

                Nothing ->
                    ( model, Cmd.none )

        UrlChange location ->
            -- This time nothing really should happen if you change url
            ( model, Cmd.none )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Platform.Sub.batch
        [ WebSocket.listen (wsUrl model) NewMessage
        , every (2 * second) Tick
        ]



-- VIEW


view : Model -> Html Msg
view model =
    let
        errorContent =
            case model.error of
                Just errMsg ->
                    [ div [ class "alert alert-dismissible alert-danger" ]
                        [ button [ type_ "button", class "close", onClick DismissError ] [ text "×" ]
                        , strong [] [ text "Oh snap!" ]
                        , text (" " ++ errMsg.content)
                        ]
                    ]

                Nothing ->
                    []

        appContent =
            if not model.login then
                [ Html.form [ onSubmit Login, class "jumbotron form-horizontal" ]
                    [ h1 [] [ text "Login" ]
                    , div [ class "form-group" ]
                        [ label [ for "user", class "col-lg-2" ] [ text "User" ]
                        , div [ class "col-lg-10" ]
                            [ input [ onInput TypeUser, value model.user, id "user", class "form-control" ] [] ]
                        ]
                    , div [ class "form-group" ]
                        [ label [ for "pass", class "col-lg-2" ] [ text "Password" ]
                        , div [ class "col-lg-10" ]
                            [ input [ type_ "password", onInput TypePass, value model.password, id "pass", class "form-control" ] [] ]
                        ]
                    , button [ type_ "submit", class "btn btn-promary" ] [ text "Login" ]
                    ]
                ]
            else
                [ div [ class "list-group" ] (List.filterMap viewTable model.tables) ]
    in
        div [ class "container" ] (appContent ++ errorContent)


viewTable : Table -> Maybe (Html Msg)
viewTable table =
    if table.beingDeleted then
        Nothing
    else
        Just
            (div [ class "list-group-item" ]
                [ h4 [ class "list-group-item-heading" ] [ text table.name ]
                , p [ class "list-group-item-text" ] [ text <| toString table.participants ]
                , p []
                    [ a [ class "btn btn-danger", onClick (DeleteTable table.id) ] [ text "Delete" ]
                    ]
                ]
            )
